const tabs = document.getElementById('tabs');
const content = document.querySelectorAll('.content');
const tabsIns = document.getElementById('tabs-ins');
const contentIns = document.querySelectorAll('.content-ins');


const changeClass = el => {
    for(let i = 0; i < tabs.children.length; i++) {
        tabs.children[i].classList.remove('active');
    }
    el.classList.add('active');
}

tabs.addEventListener('click', e => {
    const currTab = e.target.dataset.btn;
    
    changeClass(e.target);
    for(let i = 0; i < content.length; i++) {
        content[i].classList.remove('active');
        
        if(content[i].dataset.content === currTab) {
            content[i].classList.add('active');
        }
    }
})
//NEW

const changeClassIns = elI => {
    for(let n = 0; n < tabsIns.children.length; n++) {
        tabsIns.children[n].classList.remove('active');
    }
    elI.classList.add('active');
}

tabsIns.addEventListener('click', eI => {
    const currTabIns = eI.target.dataset.btnins;
    changeClassIns(eI.target);
    for(let n = 0; n < contentIns.length; n++) {
        contentIns[n].classList.remove('active');
        
        if(contentIns[n].dataset.contentins === currTabIns) {
            contentIns[n].classList.add('active');
        }
    }
})
